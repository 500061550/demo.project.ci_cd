FROM silverlogic/python3.6:latest
RUN mkdir /app
COPY . /app
WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install 
CMD /bin/sh -c "poetry install -v && poetry run python src/app/__init__.py"