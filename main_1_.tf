terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {}
variable "aws_region" { default = "us-east-1" } 
variable "aws_ecs_cluster_name" {
  default = "mid-project"
}
variable "aws_task_name" { default = "carapp-django-postgres-db" }
variable "aws_container_1_image_url" { default = "balarampratap14/postgres-cars" }
variable "aws_container_1_image_version" { default = "1.0" }
variable "aws_container_network_mode" { default = "awsvpc" }
variable "aws_container_1_name" { default = "db" }
variable "aws_container_2_image_url" { default = "balarampratap14/django-car-app" }
variable "aws_container_2_image_version" { default = "4.0" }
variable "aws_container_2_name" { default = "carapp" }
resource "aws_ecs_task_definition" "my_first_task" {
  family                   = var.aws_container_2_name 
  container_definitions    = <<DEFINITION
  [
    {
      "name": "${var.aws_container_1_name}",
      "image": "${var.aws_container_1_image_url}:${var.aws_container_1_image_version}",
      "environment" : [
    { "name" : "PGDATA", "value" : "postgres" } ],
      "essential": true,

      "memory": 512,
      "cpu": 256
    },
    {
      "name": "${var.aws_container_2_name}",
      "image": "${var.aws_container_2_image_url}:${var.aws_container_2_image_version}",
      "essential": true,
      "environment" : [
    { "name" : "allowed_host_ip", "value" : "localhost" } ],
      "portMappings": [
        {
          "containerPort": 8000,
          "hostPort": 8000,
          "protocol": "tcp"
        }
      ],
      "memory": 512,
      "cpu": 256
    }
  ]
  DEFINITION
  requires_compatibilities = ["FARGATE"] 
  network_mode             = "${var.aws_container_network_mode}"    
  memory                   = 2048         
  cpu                      = 1024       
  execution_role_arn       = "${aws_iam_role.ecsTaskExecutionRole.arn}"
}

resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = "ecsTaskExecutionRole"
  assume_role_policy = "${data.aws_iam_policy_document.assume_role_policy.json}"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_ecs_cluster" "my_cluster" {
  name = var.aws_ecs_cluster_name
}


resource "aws_default_vpc" "default_vpc" {
}

# Providing a reference to our default subnets
resource "aws_default_subnet" "default_subnet_a" {
  availability_zone = "${var.aws_region}a"
}

resource "aws_default_subnet" "default_subnet_b" {
  availability_zone = "${var.aws_region}b"
}

resource "aws_default_subnet" "default_subnet_c" {
  availability_zone = "${var.aws_region}c"
}

resource "aws_ecs_service" "my_first_service" {
  name            = "carapp-service"  
  cluster         = "${aws_ecs_cluster.my_cluster.id}"      
  task_definition = "${aws_ecs_task_definition.my_first_task.arn}" 
  launch_type     = "FARGATE"
  desired_count   = 3
  load_balancer {
    target_group_arn = "${aws_lb_target_group.target_group.arn}" # Referencing our target group
    container_name   = "${aws_ecs_task_definition.my_first_task.family}"
    container_port   = 8000 # Specifying the container port
  } 
  network_configuration {
    subnets          = ["${aws_default_subnet.default_subnet_a.id}", "${aws_default_subnet.default_subnet_b.id}", "${aws_default_subnet.default_subnet_c.id}"]
    assign_public_ip = true 
}
}

resource "aws_alb" "application_load_balancer" {
  name               = "test-lb-tf" 
  load_balancer_type = "application"
  subnets = [
    "${aws_default_subnet.default_subnet_a.id}",
    "${aws_default_subnet.default_subnet_b.id}",
    "${aws_default_subnet.default_subnet_c.id}"
  ]
  security_groups = ["${aws_security_group.load_balancer_security_group.id}"]
}

# Creating a security group for the load balancer:
resource "aws_security_group" "load_balancer_security_group" {
  ingress {
    from_port   = 8000 # Allowing traffic in from port 80
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic in from all sources
  }

  egress {
    from_port   = 0 # Allowing any incoming port
    to_port     = 0 # Allowing any outgoing port
    protocol    = "-1" # Allowing any outgoing protocol 
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
  }
}

resource "aws_lb_target_group" "target_group" {
  name        = "target-group"
  port        = 8000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${aws_default_vpc.default_vpc.id}" # Referencing the default VPC
  health_check {
    matcher = "200,301,302"
    path = "/"
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = "${aws_alb.application_load_balancer.arn}" # Referencing our load balancer
  port              = "8000"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.target_group.arn}" # Referencing our tagrte group
  }
}

resource "aws_security_group" "service_security_group" {
  ingress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    # Only allowing traffic in from the load balancer security group
    security_groups = ["${aws_security_group.load_balancer_security_group.id}"]
  }

  egress {
    from_port   = 0 # Allowing any incoming port
    to_port     = 0 # Allowing any outgoing port
    protocol    = "-1" # Allowing any outgoing protocol 
    cidr_blocks = ["0.0.0.0/0"] # Allowing traffic out to all IP addresses
  }
}
