from app import app

app.testing = True
client = app.test_client()


def test_ready():
    response = client.get('/ready')
    assert b"200" in response.data
def test_error():  
    response = client.get('/error')
    assert b"500" in response.data
