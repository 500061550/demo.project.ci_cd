from flask import Flask
app = Flask(__name__)

@app.route('/ready', methods=["GET"])
def ready():
    status_code = '200'
    return status_code
@app.route('/error', methods=["GET"])
def error():
    status_code = '500'
    return status_code
if __name__ == "__main__":
    app.run(debug=True,host="0.0.0.0", port="5000")
